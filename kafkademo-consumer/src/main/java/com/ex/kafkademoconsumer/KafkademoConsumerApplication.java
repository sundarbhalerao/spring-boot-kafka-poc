package com.ex.kafkademoconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkademoConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkademoConsumerApplication.class, args);
	}

}
