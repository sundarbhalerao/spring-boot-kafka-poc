package com.ex.kafkademo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ex.kafkademo.model.User;

@RestController
@RequestMapping("kafka")
public class UserController {

	@Autowired
	private KafkaTemplate<String , User> kafkaTemplate;
	
	private static final String TOPIC = "Kafka_Demo";
	
	@GetMapping("/publish/user")
	public String publishMessage(@RequestBody User user) {
		
		kafkaTemplate.send(TOPIC , user);
		
		return "Message Published ...";
	}
}
