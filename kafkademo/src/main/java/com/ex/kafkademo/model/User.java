package com.ex.kafkademo.model;

public class User {

	private String name;
	private String Address;
	private long phoneNo;
	
	public User() {}
	
	public User(String name, String address, long phoneNo) {
		super();
		this.name = name;
		Address = address;
		this.phoneNo = phoneNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	
	
}
